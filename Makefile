ENV?=${USER}
APP_NAME?=${ENV}-budget

.PHONY: format
format:
	pipenv run black .

.PHONY: lint
lint:
	pipenv run flake8 .

.PHONY: test
test:
	pipenv run pytest --junitxml=report.xml

.PHONY: local
local:
	pipenv run uvicorn app.app:app --reload

requirements.txt:
	pipenv lock -r > requirements.txt

.PHONY: deploy
deploy:
	git push -f git@heroku.com:${APP_NAME}.git HEAD:refs/heads/main
