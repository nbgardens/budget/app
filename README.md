# Budget Backend App
This is the backend for the budget application. It is built using FastAPI and is run on heroku.

The live application can be found at:
https://live-budget.herokuapp.com

And the API Documentation can be found at:
https://live-budget.herokuapp.com/docs
