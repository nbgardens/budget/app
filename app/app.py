from fastapi import Depends, FastAPI
from app.db.db import create_db_and_tables
from app.db.models import UserDB
from app.db.users import (
    auth_backend,
    current_active_user,
    fastapi_users,
    current_user,
)

app = FastAPI()

app.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/auth/cookie",
    tags=["auth"],
)
app.include_router(
    fastapi_users.get_register_router(), prefix="/auth", tags=["auth"]
)
app.include_router(
    fastapi_users.get_reset_password_router(),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    fastapi_users.get_verify_router(),
    prefix="/auth",
    tags=["auth"],
)
app.include_router(
    fastapi_users.get_users_router(), prefix="/users", tags=["users"]
)


@app.get("/authenticated-route")
async def authenticated_route(user: UserDB = Depends(current_active_user)):
    return {"message": f"Hello {user.email}!"}


@app.get("/")
async def root(user: UserDB = Depends(current_user)):
    if user:
        return {"message": f"Hello {user.email}!"}
    return {"message": "Please login"}


@app.on_event("startup")
async def on_startup():
    await create_db_and_tables()
