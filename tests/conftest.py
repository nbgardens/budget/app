from app.app import app
from app.db.users import current_active_user
from app.db.models import UserDB
from fastapi.testclient import TestClient
from pydantic import EmailStr
import pytest
import uuid


@pytest.fixture
def client():
    """
    Return an API Client
    """
    app.dependency_overrides = {}
    return TestClient(app)


@pytest.fixture
def client_authenticated():
    """
    Returns an API client which skips the authentication
    """

    def skip_auth():
        test_user = UserDB(
            id=uuid.uuid4(),
            username="test_user",
            hashed_password="hashed_password",
            email=EmailStr("mail@mail.com"),
            is_active=True,
            is_superuser=False,
            is_verified=True,
        )
        return test_user

    app.dependency_overrides[current_active_user] = skip_auth
    return TestClient(app)
