def test_authentication(client_authenticated):
    response = client_authenticated.get("/authenticated-route")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello mail@mail.com!"}


def test_unauthorized(client):
    response = client.get("/authenticated-route")
    assert response.status_code == 401
    assert response.json() == {"detail": "Unauthorized"}
